package folder.part1.hospital;

import folder.part1.hospital.healthpersonnel.Nurse;
import folder.part1.hospital.healthpersonnel.doctor.GeneralPractitioner;
import folder.part1.hospital.healthpersonnel.doctor.Surgeon;

public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", 10));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", 20));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", 30));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", 40));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", 50));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", 60));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", 70));
        emergency.getPatients().add(new Patient("Inga", "Lykke", 80));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", 90));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", 100));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", 10));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", 10));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", 110));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", 120));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", 130));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", 140));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", 150));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", 160));
        hospital.getDepartments().add(childrenPolyclinic);
    }
}