package folder.part1.hospital;

public interface Diagnosable {
    void setDiagnosis(String diagnosis);
}
