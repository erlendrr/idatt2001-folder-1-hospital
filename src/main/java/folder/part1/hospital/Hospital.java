package folder.part1.hospital;

import java.util.ArrayList;
import java.util.Iterator;

public class Hospital {
    private final String name;
    private ArrayList<Department> departments;

    public Hospital(String name) {
        this.name = name;
        this.departments = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public ArrayList<Department> getDepartments() {
        return departments;
    }

    public Department getDepartment(String departmentName) {
        Iterator<Department> itr1 = departments.iterator();
        while (itr1.hasNext()) {
            Department department1 = itr1.next();
            if (department1.getName().equals(departmentName)) {
                return department1;
            }
        }
        return null;
    }

    /**
     * Method for adding department to department-register
     *
     * @param department any department-object
     */
    public void addDepartment(Department department) {
        departments.add(department);
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "name='" + name + '\'' +
                ", departments=" + departments +
                '}';
    }
}
