package folder.part1.hospital.exception;

public class RemoveException extends Exception {
    private final static long serialVersionUID = 1L;
    public RemoveException(String exception) {
        super(exception);
    }
}
