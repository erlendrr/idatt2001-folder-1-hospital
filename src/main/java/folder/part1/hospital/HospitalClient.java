package folder.part1.hospital;

import folder.part1.hospital.healthpersonnel.Nurse;

public class HospitalClient {
    public static void main(String[] args) {
        /*
        Lag klienten HospitalClient som bruker HospitalTestData.fillRegisterWithTestData(hospital) til å
        */
        Hospital hospital = new Hospital("Sykehus-navn");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println(hospital);

        /*
        Kall remove metoden på Department for å fjerne en ansatt
        */
        try {
            hospital.getDepartment("Akutten").remove(new Nurse("Ove", "Ralt", 70));
        }
        catch (Exception exception) {
            System.out.println(exception);
        }

        /*
        Kall remove metoden på Department for å fjerne en pasient som ikke finnes i listen. Bruk try‐
        catch exception blokk for å håndtere situasjonen.
        */
        try {
            hospital.getDepartment("Akutten").remove(new Patient("Karl", "Gregory", 6136));
        }
        catch (Exception exception) {
            System.out.println(exception);
        }
    }
}
