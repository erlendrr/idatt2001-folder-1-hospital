package folder.part1.hospital;

import folder.part1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class Department {
    protected String name;
    protected ArrayList<Employee> employees;
    protected ArrayList<Patient> patients;

    public Department(String name) {
        this.name = name;
        this.employees = new ArrayList<>();
        this.patients = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * Method for adding employee to employee-register
     *
     * @param employee Can accept any employee-object
     */
    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    /**
     *
     * Method for adding patient to patient-register
     *
     * @param patient Can accept any patient-object
     */
    public void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     *
     * Method for removing person-object from the register
     *
     * @param person Can accept any object that is inherited from the person class
     * @throws RemoveException Throws an exception if person does not exist
     */
    public void remove(Person person) throws RemoveException {
        if (person == null) {
            throw new IllegalArgumentException("Can not accept null-object");
        }
        Iterator<Employee> itr1 = employees.iterator();
        while (itr1.hasNext()) {
            Employee employee1 = itr1.next();
            if (employee1.getSocialSecurityNumber() == person.getSocialSecurityNumber()) {
                itr1.remove();
                System.out.println("Person: '" + person.getFullName() + "' has been removed from " + this.name);
                return;
            }
        }
        Iterator<Patient> itr2 = patients.iterator();
        while (itr2.hasNext()) {
            Patient patient1 = itr2.next();
            if (patient1.getSocialSecurityNumber() == person.getSocialSecurityNumber()) {
                itr2.remove();
                System.out.println("Person: '" + person.getFullName() + "' has been removed from " + this.name);
                return;
            }
        }
        throw new RemoveException("Person: '" + person.getFullName() + "' finnes ikke i pasient-, eller ansatt-registeret på avdeling: '" + this.name + "'");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Objects.equals(name, that.name) && Objects.equals(employees, that.employees) && Objects.equals(patients, that.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, employees, patients);
    }

    @Override
    public String toString() {
        return "Department{" +
                "name='" + name + '\'' +
                ", employees=" + employees +
                ", patients=" + patients +
                '}';
    }
}
