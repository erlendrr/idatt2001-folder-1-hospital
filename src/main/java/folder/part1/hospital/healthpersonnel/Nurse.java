package folder.part1.hospital.healthpersonnel;

import folder.part1.hospital.Employee;

public class Nurse extends Employee {
    public Nurse(String firstName, String lastName, int socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
