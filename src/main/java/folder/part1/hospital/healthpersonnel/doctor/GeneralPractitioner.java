package folder.part1.hospital.healthpersonnel.doctor;

import folder.part1.hospital.Patient;

public class GeneralPractitioner extends Doctor {
    public GeneralPractitioner(String firstName, String lastName, int socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
