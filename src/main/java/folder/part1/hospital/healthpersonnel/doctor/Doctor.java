package folder.part1.hospital.healthpersonnel.doctor;

import folder.part1.hospital.Employee;

abstract  class Doctor extends Employee {

    public Doctor(String firstName, String lastName, int socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
}
