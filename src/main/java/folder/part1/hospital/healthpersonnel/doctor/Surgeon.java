package folder.part1.hospital.healthpersonnel.doctor;

import folder.part1.hospital.Patient;

public class Surgeon extends Doctor {
    public Surgeon(String firstName, String lastName, int socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }
    public void setDiagnosis(Patient patient, String diagnosis) {
        patient.setDiagnosis(diagnosis);
    }
}
