package folder.part1.hospital;

import folder.part1.hospital.exception.RemoveException;
import folder.part1.hospital.healthpersonnel.Nurse;
import folder.part1.hospital.healthpersonnel.doctor.GeneralPractitioner;
import folder.part1.hospital.healthpersonnel.doctor.Surgeon;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;


class DepartmentTest {
    @Test
    @DisplayName("Should not remove a person")
    void shouldNotRemovePerson() {
        Department department1 = new Department("Department-Name");
        Employee testPerson = new Employee("Ola", "Nordmann", 123456);

        assertThrows(RemoveException.class, () -> department1.remove(testPerson));
    }

    @Test
    @DisplayName("Test for unwanted input")
    void shouldReturnNull() {
        Department department1 = new Department("Department-Name");
        assertThrows(IllegalArgumentException.class, () -> department1.remove(null));
    }

    @Test
    @DisplayName("Should remove a person")
    void shouldRemovePerson() {
        Department department1 = new Department("Department-Name");

        Employee employee1 = new Employee("Olav", "Hestersen", 10);
        department1.addEmployee(employee1);

        Nurse nurse1 = new Nurse("Frida", "Guffe", 20);
        department1.addEmployee(nurse1);


        Surgeon surgeon1 = new Surgeon("Trond", "Potetlars", 30);
        department1.addEmployee(surgeon1);

        GeneralPractitioner generalPractitioner1 = new GeneralPractitioner("Lisa", "Lisboa", 40);
        department1.addEmployee(generalPractitioner1);

        Patient patient1 = new Patient("Hans", "Torgeir", 50);
        department1.addPatient(patient1);


        assertDoesNotThrow(() -> {
            department1.remove(employee1);
            department1.remove(nurse1);
            department1.remove(surgeon1);
            department1.remove(generalPractitioner1);
            department1.remove(patient1);
        });
    }
}
