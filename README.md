# idatt2001-folder-1-hospital

## Description

Maven-application for creating a registry of patients and employees in a hospital

[Link to task-description](https://learn-eu-central-1-prod-fleet01-xythos.content.blackboardcdn.com/5def77a38a2f7/8769197?X-Blackboard-Expiration=1615431600000&X-Blackboard-Signature=5R%2FEPzD2rLs97RHE0eKqH0579uPklT6NNpVOQMH9KSI%3D&X-Blackboard-Client-Id=303508&response-cache-control=private%2C%20max-age%3D21600&response-content-disposition=inline%3B%20filename%2A%3DUTF-8%27%27IDATx2001_Mappe_Del_1%25281%2529.pdf&response-content-type=application%2Fpdf&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Date=20210310T210000Z&X-Amz-SignedHeaders=host&X-Amz-Expires=21600&X-Amz-Credential=AKIAZH6WM4PL5M5HI5WH%2F20210310%2Feu-central-1%2Fs3%2Faws4_request&X-Amz-Signature=bedf6de790ddc6d5c1b3c8b00764eeebc4b6165f6715b0cde259d4220794b0a8)

## Testing
```
mvn test
```
## Building
```
mvn install
```

## Kommentarer
Til oppgaven: I etterkant, ser jeg at jeg burde ha hatt mer deskriptive commit-navn, som for eksmpel istedenfor task-2, kunne jeg hatt:
task-2: Implementing personnel-classes.

Jeg kunne også ha beskrevet flere metoder i JavaDoc,
men jeg mener at koden er såpass oversiktlig at dette ikke er nødvendig.
